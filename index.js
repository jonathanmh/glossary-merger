var time1 = new Date().getTime();

var fs = require('fs');
var async = require('async');
var jsonfm = require('json-front-matter');
var marked = require('marked');
var nconf = require('nconf');
var wordpress = require('wordpress');

nconf.file({ file: process.cwd() + '/.gmerge.json' });

var path = process.cwd() + '/' + nconf.get('path');
var prependTitle = nconf.get('prependTitle');
;
var client = wordpress.createClient({
  url: nconf.get("url"),
  username: nconf.get("username"),
  password: nconf.get("password")
});

var output = '';
var data = {};
var files = [];
var glossary = [];
var subTopics = [];
console.log(path)

fs.readdir(path, function(err, files){
  console.log(files)
  async.each(files, function(file, callback){
    jsonfm.parseFile(path+file, function(err, parsed){
      glossary.push(parsed);
      callback();
    });
  }, function(err){
    if(err){
      console.log(err);
    }
    else {
      var i = 0;
      async.each(glossary, function(item, callback){
        if(item.attributes.hasOwnProperty('parent') && item.attributes.parent != undefined){
          subTopics.push(item);
        }
        if(item.attributes.title == 'intro'){
          output += item.body;
        }
        i++;
        callback();
      }, function(err){
        if(err){
          console.log(err);
        }

        glossary.sort(function(a,b){
          if(a.attributes.title < b.attributes.title){
            return -1;
          }
          if(a.attributes.title > b.attributes.title){
            return 1
          }
          else {
            return 0;
          }
        });

        subTopics.sort(function(a,b){
          if(a.attributes.title < b.attributes.title){
            return -1;
          }
          if(a.attributes.title > b.attributes.title){
            return 1;
          }
          else {
            return 0;
          }
        });

        for(i = 0; i < glossary.length; i++){
          if(glossary[i].attributes.parent == undefined && glossary[i].attributes.hidden != true){
            output += '## ' + glossary[i].attributes.title + '\n';
            console.log('## ' + glossary[i].attributes.title + '\n');
            output += glossary[i].body + '\n';
          }

          for(j = 0; j < subTopics.length; j++){
            if(subTopics[j].attributes.parent == glossary[i].attributes.title && subTopics[j].attributes.hidden != true){
              output += '### ' + subTopics[j].attributes.title + '\n';
              console.log('### ' + subTopics[j].attributes.title + '\n');
              output += subTopics[j].body + '\n';
            }
          }
        }

        if(process.argv[2] != "--offline"){
          data =  {
            "content": marked(output)
          }
          client.editPost(nconf.get('postId'), data, function(error){
            if(error){
              console.log(error);
            }
            else {
              console.log("page successfully updated");
            }
          });
        }

        fs.writeFile('output.md', output, function (err) {
          if (err) throw err;
          var time2 = new Date().getTime();
          var diff = time2 - time1;
          console.log('this took ' + diff + ' ms');
        });
      });
    }
  });
});
