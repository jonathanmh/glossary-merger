# Glossary Merger

A small tool to grab a bunch of markdown files, sort them alphabetically and append the respective sub-topics.

I was using [mdclip](http://jonathanmh.com/copy-markdown-html-to-clipboard-from-the-terminal-shell/) to compile this to html and put it on my blog, but now you can specify a post ID and it will auto update the page with compiled HTML.

## Installation
```
npm install
```

## Usage
with a config file present (.gmerge in same directory):
```
node index.js
```

if you only want to produce output.md, use:

```
node --offline index.js
```

Folder structure:
```
glossary/
- icecream.md
- cholocate.md
```

Example file:
```
{{{
  "title": "Chocolate",
  "parent": "Icecream", (optional)
  "hidden": true, (optional)
}}}
```

## ToDo
* build table of contents
* make the headline levels adjustable
* make the code pretty

## Done
* automatic upload to wordpress
* create intro text functionality
* disable files with attribute `hidden`
* migrate settings to nconf
